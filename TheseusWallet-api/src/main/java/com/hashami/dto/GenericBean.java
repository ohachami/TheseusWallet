package com.hashami.dto;

import java.io.Serializable;

public class GenericBean implements Serializable{

	private String tokenId;

	/**
	 * @return the tokenId
	 */
	public String getTokenId() {
		return tokenId;
	}

	/**
	 * @param tokenId the tokenId to set
	 */
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	
	
}
