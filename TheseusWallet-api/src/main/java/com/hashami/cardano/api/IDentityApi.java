package com.hashami.cardano.api;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.hashami.dto.UserBean;

@Path("/")
public interface IDentityApi {

    @POST
    @Path("/users")
    @Consumes({ "application/json;charset=utf-8" })
    @Produces({ "application/json;charset=utf-8" })
    Response registerUser(@NotNull @Valid UserBean auth);
    
    @POST
    @Path("/login")
    @Consumes({ "application/json;charset=utf-8" })
    @Produces({ "application/json;charset=utf-8" })
    Response authenticateUser(@NotNull @Valid  UserBean auth);
	
}
