package com.hashami.cardano.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.hashami.cardano.dto.NewAccount;
import com.hashami.cardano.dto.NewAddress;
import com.hashami.cardano.dto.Payment;
import com.hashami.cardano.dto.Wallet;

@Path("/v1")
public interface ICardanoApi {

    @POST
    @Path("/wallets")
    @Consumes({ "application/json;charset=utf-8" })
    @Produces({ "application/json;charset=utf-8" })
    Response createWallet(Wallet wallet);
    
    @GET
    @Path("/wallets")
    @Produces({ "application/json;charset=utf-8" })
    Response getWallet(@QueryParam("walletId")String id);
    
    @GET
    @Path("/wallets/accounts")
    @Consumes({ "application/json;charset=utf-8" })
    @Produces({ "application/json;charset=utf-8" })   
    Response getAccounts();
    
    @GET
    @Path("/wallets/user/accounts")
    @Consumes({ "application/json;charset=utf-8" })
    @Produces({ "application/json;charset=utf-8" })   
    Response getUserAccounts();
    
    @POST
    @Path("/wallets/accounts")
    @Consumes({ "application/json;charset=utf-8" })
    @Produces({ "application/json;charset=utf-8" })
    Response createAccount(NewAccount body);
    
    @GET
    @Path("/addresses/{address}/validity")
    @Produces({ "application/json;charset=utf-8" })
    Response verifyAddress(@PathParam("address") String address);
    
    @POST
    @Path("/addresses")
    @Consumes({ "application/json;charset=utf-8" })
    @Produces({ "application/json;charset=utf-8" })
    Response createAddress(NewAddress body);
    
    @POST
    @Path("/transactions/batch")
    @Consumes({ "application/json;charset=utf-8" })
    @Produces({ "application/json;charset=utf-8" })
    Response createBatchTransactions(Payment body);
    
    @POST
    @Path("/transactions")
    @Consumes({ "application/json;charset=utf-8" })
    @Produces({ "application/json;charset=utf-8" })
    Response createTransaction(Payment payment);
    
    @GET
    @Path("/transactions/{walletId}")
    @Produces({ "application/json;charset=utf-8" })
    Response searchTransactions(@QueryParam("address")String address, @QueryParam("page")@DefaultValue("1") Integer page, @QueryParam("per_page")@DefaultValue("10") Integer perPage);    
}
