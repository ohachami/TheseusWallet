package com.hashami.cardano.dto;

public class WalletUpdate  {
  

public enum AssuranceLevelEnum {

	NORMAL(String.valueOf("normal")), STRICT(String.valueOf("strict"));


    private String value;

    AssuranceLevelEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static AssuranceLevelEnum fromValue(String v) {
        for (AssuranceLevelEnum b : AssuranceLevelEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

 /**
   * New assurance level  
  **/
  private AssuranceLevelEnum assuranceLevel = null;

 /**
   * New wallet's name  
  **/
  private String name = null;
 /**
   * New assurance level
   * @return assuranceLevel
  **/
  public String getAssuranceLevel() {
    if (assuranceLevel == null) {
      return null;
    }
    return assuranceLevel.value();
  }

  public void setAssuranceLevel(AssuranceLevelEnum assuranceLevel) {
    this.assuranceLevel = assuranceLevel;
  }

  public WalletUpdate assuranceLevel(AssuranceLevelEnum assuranceLevel) {
    this.assuranceLevel = assuranceLevel;
    return this;
  }

 /**
   * New wallet&#39;s name
   * @return name
  **/
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public WalletUpdate name(String name) {
    this.name = name;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WalletUpdate {\n");
    
    sb.append("    assuranceLevel: ").append(toIndentedString(assuranceLevel)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

