package com.hashami.cardano.dto;

import java.math.BigDecimal;

public class PaginationMetadata  {
  
  private BigDecimal totalPages = null;

  private Integer page = null;

  private Integer perPage = null;

  private BigDecimal totalEntries = null;
 /**
   * Get totalPages
   * minimum: 0
   * maximum: 9223372036854776000
   * @return totalPages
  **/
  public BigDecimal getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(BigDecimal totalPages) {
    this.totalPages = totalPages;
  }

  public PaginationMetadata totalPages(BigDecimal totalPages) {
    this.totalPages = totalPages;
    return this;
  }

 /**
   * Get page
   * @return page
  **/
  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public PaginationMetadata page(Integer page) {
    this.page = page;
    return this;
  }

 /**
   * Get perPage
   * @return perPage
  **/
  public Integer getPerPage() {
    return perPage;
  }

  public void setPerPage(Integer perPage) {
    this.perPage = perPage;
  }

  public PaginationMetadata perPage(Integer perPage) {
    this.perPage = perPage;
    return this;
  }

 /**
   * Get totalEntries
   * minimum: 0
   * maximum: 9223372036854776000
   * @return totalEntries
  **/
  public BigDecimal getTotalEntries() {
    return totalEntries;
  }

  public void setTotalEntries(BigDecimal totalEntries) {
    this.totalEntries = totalEntries;
  }

  public PaginationMetadata totalEntries(BigDecimal totalEntries) {
    this.totalEntries = totalEntries;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaginationMetadata {\n");
    
    sb.append("    totalPages: ").append(toIndentedString(totalPages)).append("\n");
    sb.append("    page: ").append(toIndentedString(page)).append("\n");
    sb.append("    perPage: ").append(toIndentedString(perPage)).append("\n");
    sb.append("    totalEntries: ").append(toIndentedString(totalEntries)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

