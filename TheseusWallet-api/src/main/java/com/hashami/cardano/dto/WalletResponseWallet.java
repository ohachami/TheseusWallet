package com.hashami.cardano.dto;

import java.util.ArrayList;
import java.util.List;

public class WalletResponseWallet  {
  
  private List<Wallet> data = new ArrayList<>();

  private ResponseStatus status = null;

  private Metadata meta = null;
 /**
   * Get data
   * @return data
  **/
  public List<Wallet> getData() {
    return data;
  }

  public void setData(List<Wallet> data) {
    this.data = data;
  }

  public WalletResponseWallet data(List<Wallet> data) {
    this.data = data;
    return this;
  }

  public WalletResponseWallet addDataItem(Wallet dataItem) {
    this.data.add(dataItem);
    return this;
  }

 /**
   * Get status
   * @return status
  **/
  public ResponseStatus getStatus() {
    return status;
  }

  public void setStatus(ResponseStatus status) {
    this.status = status;
  }

  public WalletResponseWallet status(ResponseStatus status) {
    this.status = status;
    return this;
  }

 /**
   * Get meta
   * @return meta
  **/
  public Metadata getMeta() {
    return meta;
  }

  public void setMeta(Metadata meta) {
    this.meta = meta;
  }

  public WalletResponseWallet meta(Metadata meta) {
    this.meta = meta;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class WalletResponseWallet {\n");
    
    sb.append("    data: ").append(toIndentedString(data)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    meta: ").append(toIndentedString(meta)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

