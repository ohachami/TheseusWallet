package com.hashami.cardano.dto;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

public class Payment  {
  

  private PaymentSource source = null;
 /**
   * One or more destinations for the payment  
  **/
	@Valid
  private List<PaymentDistribution> destinations = new ArrayList<PaymentDistribution>();


public enum GroupingPolicyEnum {

	OPTIMIZEFORSECURITY(String.valueOf("OptimizeForSecurity")), OPTIMIZEFORHIGHTHROUGHPUT(String.valueOf("OptimizeForHighThroughput"));

    private String value;

    GroupingPolicyEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static GroupingPolicyEnum fromValue(String v) {
        for (GroupingPolicyEnum b : GroupingPolicyEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

 /**
   * Optional strategy to use for selecting the transaction inputs  
  **/
  private GroupingPolicyEnum groupingPolicy = null;

 /**
   * Optional spending password to access funds  
  **/
  private String spendingPassword = null;

 /**
   * One or more destinations for the payment
   * @return destinations
  **/
  public List<PaymentDistribution> getDestinations() {
    return destinations;
  }

  public void setDestinations(List<PaymentDistribution> destinations) {
    this.destinations = destinations;
  }

  public Payment destinations(List<PaymentDistribution> destinations) {
    this.destinations = destinations;
    return this;
  }

  public Payment addDestinationsItem(PaymentDistribution destinationsItem) {
    this.destinations.add(destinationsItem);
    return this;
  }

 /**
   * Optional strategy to use for selecting the transaction inputs
   * @return groupingPolicy
  **/
  public String getGroupingPolicy() {
    if (groupingPolicy == null) {
      return null;
    }
    return groupingPolicy.value();
  }

  public void setGroupingPolicy(GroupingPolicyEnum groupingPolicy) {
    this.groupingPolicy = groupingPolicy;
  }

  public Payment groupingPolicy(GroupingPolicyEnum groupingPolicy) {
    this.groupingPolicy = groupingPolicy;
    return this;
  }

 /**
   * Optional spending password to access funds
   * @return spendingPassword
  **/
  public String getSpendingPassword() {
    return spendingPassword;
  }

  public void setSpendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
  }

  public Payment spendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Payment {\n");
    
    sb.append("    destinations: ").append(toIndentedString(destinations)).append("\n");
    sb.append("    groupingPolicy: ").append(toIndentedString(groupingPolicy)).append("\n");
    sb.append("    spendingPassword: ").append(toIndentedString(spendingPassword)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

/**
 * @return the source
 */
public PaymentSource getSource() {
	return source;
}

/**
 * @param source the source to set
 */
public void setSource(PaymentSource source) {
	this.source = source;
}
}

