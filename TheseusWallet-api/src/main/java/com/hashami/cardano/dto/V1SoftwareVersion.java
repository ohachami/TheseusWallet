package com.hashami.cardano.dto;

public class V1SoftwareVersion  {
  
  private String applicationName = null;

  private Integer version = null;
 /**
   * Get applicationName
   * @return applicationName
  **/
  public String getApplicationName() {
    return applicationName;
  }

  public void setApplicationName(String applicationName) {
    this.applicationName = applicationName;
  }

  public V1SoftwareVersion applicationName(String applicationName) {
    this.applicationName = applicationName;
    return this;
  }

 /**
   * Get version
   * minimum: 0
   * maximum: 4294967295
   * @return version
  **/
  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public V1SoftwareVersion version(Integer version) {
    this.version = version;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class V1SoftwareVersion {\n");
    
    sb.append("    applicationName: ").append(toIndentedString(applicationName)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

