package com.hashami.cardano.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.hashami.dto.UserBean;

public class Account  {
  
 /**
   * Account's index in the wallet, starting at 0  
  **/
  private Long index = null;

 /**
   * Public addresses pointing to this account  
  **/
  private List<WalletAddress> addresses = new ArrayList<WalletAddress>();

 /**
   * Available funds, in ADA  
  **/
  private BigDecimal amount = null;

 /**
   * Account's name  
  **/
  private String name = null;

 /**
   * Id of the wallet this account belongs to  
  **/
  private String walletId = null;
  
  private UserBean user;
 /**
   * Account&#39;s index in the wallet, starting at 0
   * minimum: 0
   * maximum: 4294967295
   * @return index
  **/
  public Long getIndex() {
    return index;
  }

  public void setIndex(Long index) {
    this.index = index;
  }

  public Account index(Long index) {
    this.index = index;
    return this;
  }

 /**
   * Public addresses pointing to this account
   * @return addresses
  **/
  public List<WalletAddress> getAddresses() {
    return addresses;
  }

  public void setAddresses(List<WalletAddress> addresses) {
    this.addresses = addresses;
  }

  public Account addresses(List<WalletAddress> addresses) {
    this.addresses = addresses;
    return this;
  }

  public Account addAddressesItem(WalletAddress addressesItem) {
    this.addresses.add(addressesItem);
    return this;
  }

 /**
   * Available funds, in ADA
   * maximum: 45000000000000000
   * @return amount
  **/
  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public Account amount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }

 /**
   * Account&#39;s name
   * @return name
  **/
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Account name(String name) {
    this.name = name;
    return this;
  }

 /**
   * Id of the wallet this account belongs to
   * @return walletId
  **/
  public String getWalletId() {
    return walletId;
  }

  public void setWalletId(String walletId) {
    this.walletId = walletId;
  }

  public Account walletId(String walletId) {
    this.walletId = walletId;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Account {\n");
    
    sb.append("    index: ").append(toIndentedString(index)).append("\n");
    sb.append("    addresses: ").append(toIndentedString(addresses)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    walletId: ").append(toIndentedString(walletId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

/**
 * @return the user
 */
public UserBean getUser() {
	return user;
}

/**
 * @param user the user to set
 */
public void setUser(UserBean user) {
	this.user = user;
}

}

