package com.hashami.cardano.dto;

public class NewAddress  {
  
 /**
   * Optional spending password to unlock funds  
  **/
  private String spendingPassword = null;

 /**
   * Target account's index to store this address in  
  **/
  private Long accountIndex = null;

 /**
   * Corresponding wallet identifier  
  **/
  private String walletId = null;
 /**
   * Optional spending password to unlock funds
   * @return spendingPassword
  **/
  public String getSpendingPassword() {
    return spendingPassword;
  }

  public void setSpendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
  }

  public NewAddress spendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
    return this;
  }

 /**
   * Target account&#39;s index to store this address in
   * minimum: 0
   * maximum: 4294967295
   * @return accountIndex
  **/
  public Long getAccountIndex() {
    return accountIndex;
  }

  public void setAccountIndex(Long accountIndex) {
    this.accountIndex = accountIndex;
  }

  public NewAddress accountIndex(Long accountIndex) {
    this.accountIndex = accountIndex;
    return this;
  }

 /**
   * Corresponding wallet identifier
   * @return walletId
  **/
  public String getWalletId() {
    return walletId;
  }

  public void setWalletId(String walletId) {
    this.walletId = walletId;
  }

  public NewAddress walletId(String walletId) {
    this.walletId = walletId;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NewAddress {\n");
    
    sb.append("    spendingPassword: ").append(toIndentedString(spendingPassword)).append("\n");
    sb.append("    accountIndex: ").append(toIndentedString(accountIndex)).append("\n");
    sb.append("    walletId: ").append(toIndentedString(walletId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

