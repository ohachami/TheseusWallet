package com.hashami.cardano.dto;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

public class PaymentV0 {
	
	@NotNull
	private String fromAccount;
	@NotNull
	private String toAddr;
	@NotNull
	private BigDecimal amount;

	
	/**
	 * @return the toAddr
	 */
	public String getToAddr() {
		return toAddr;
	}
	/**
	 * @param toAddr the toAddr to set
	 */
	public void setToAddr(String toAddr) {
		this.toAddr = toAddr;
	}
	/**
	 * @return the amount
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * @return the fromAccount
	 */
	public String getFromAccount() {
		return fromAccount;
	}
	/**
	 * @param fromAccount the fromAccount to set
	 */
	public void setFromAccount(String fromAccount) {
		this.fromAccount = fromAccount;
	}
	
	
}
