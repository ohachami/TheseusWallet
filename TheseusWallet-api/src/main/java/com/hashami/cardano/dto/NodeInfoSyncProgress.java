package com.hashami.cardano.dto;

import java.math.BigDecimal;

/**
  * Syncing progression, in percentage
 **/
public class NodeInfoSyncProgress  {
  
  private BigDecimal quantity = null;


public enum UnitEnum {

	PERCENT(String.valueOf("percent"));

    private String value;

    UnitEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static UnitEnum fromValue(String v) {
        for (UnitEnum b : UnitEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

  private UnitEnum unit = null;
 /**
   * Get quantity
   * minimum: 0
   * maximum: 100
   * @return quantity
  **/
  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public NodeInfoSyncProgress quantity(BigDecimal quantity) {
    this.quantity = quantity;
    return this;
  }

 /**
   * Get unit
   * @return unit
  **/
  public String getUnit() {
    if (unit == null) {
      return null;
    }
    return unit.value();
  }

  public void setUnit(UnitEnum unit) {
    this.unit = unit;
  }

  public NodeInfoSyncProgress unit(UnitEnum unit) {
    this.unit = unit;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NodeInfoSyncProgress {\n");
    
    sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
    sb.append("    unit: ").append(toIndentedString(unit)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

