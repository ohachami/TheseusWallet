package com.hashami.cardano.dto;

/**
  * Source for the payment
 **/
public class PaymentSource  {
  
 /**
   * Corresponding account's index on the wallet  
  **/
  private Long accountIndex = null;

 /**
   * Target wallet identifier to reach  
  **/
  private String walletId = null;
 /**
   * Corresponding account&#39;s index on the wallet
   * minimum: 0
   * maximum: 4294967295
   * @return accountIndex
  **/
  public Long getAccountIndex() {
    return accountIndex;
  }

  public void setAccountIndex(Long accountIndex) {
    this.accountIndex = accountIndex;
  }

  public PaymentSource accountIndex(Long accountIndex) {
    this.accountIndex = accountIndex;
    return this;
  }

 /**
   * Target wallet identifier to reach
   * @return walletId
  **/
  public String getWalletId() {
    return walletId;
  }

  public void setWalletId(String walletId) {
    this.walletId = walletId;
  }

  public PaymentSource walletId(String walletId) {
    this.walletId = walletId;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentSource {\n");
    
    sb.append("    accountIndex: ").append(toIndentedString(accountIndex)).append("\n");
    sb.append("    walletId: ").append(toIndentedString(walletId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

