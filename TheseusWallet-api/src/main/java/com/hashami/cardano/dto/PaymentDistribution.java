package com.hashami.cardano.dto;

import java.math.BigDecimal;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

public class PaymentDistribution  {
  
 /**
   * Address to map coins to  
  **/
  @NotNull
  private String address = null;

 /**
   * Amount of coin to bind, in ADA  
  **/
  @NotNull
  @DecimalMin(value="0", inclusive=false)
  private BigDecimal amount = null;
 /**
   * Address to map coins to
   * @return address
  **/
  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public PaymentDistribution address(String address) {
    this.address = address;
    return this;
  }

 /**
   * Amount of coin to bind, in ADA
   * maximum: 45000000000000000
   * @return amount
  **/
  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public PaymentDistribution amount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentDistribution {\n");
    
    sb.append("    address: ").append(toIndentedString(address)).append("\n");
    sb.append("    amount: ").append(toIndentedString(amount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

