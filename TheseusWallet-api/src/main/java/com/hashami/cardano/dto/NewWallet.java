package com.hashami.cardano.dto;

import java.util.ArrayList;
import java.util.List;

public class NewWallet  {
  
 /**
   * Backup phrase to restore the wallet  
  **/
  private List<String> backupPhrase = new ArrayList<String>();

 /**
   * Optional spending password to encrypt / decrypt private keys  
  **/
  private String spendingPassword = null;


public enum AssuranceLevelEnum {

	NORMAL(String.valueOf("normal")), STRICT(String.valueOf("strict"));


    private String value;

    AssuranceLevelEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static AssuranceLevelEnum fromValue(String v) {
        for (AssuranceLevelEnum b : AssuranceLevelEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

 /**
   * Desired assurance level based on the number of confirmations counter of each transaction.  
  **/
  private AssuranceLevelEnum assuranceLevel = null;

 /**
   * Wallet's name  
  **/
  private String name = null;


public enum OperationEnum {

	CREATE(String.valueOf("create")), RESTORE(String.valueOf("restore"));


    private String value;

    OperationEnum (String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static OperationEnum fromValue(String v) {
        for (OperationEnum b : OperationEnum.values()) {
            if (String.valueOf(b.value).equals(v)) {
                return b;
            }
        }
        return null;
    }
}

 /**
   * Create a new wallet or Restore an existing one  
  **/
  private OperationEnum operation = null;
 /**
   * Backup phrase to restore the wallet
   * @return backupPhrase
  **/
  public List<String> getBackupPhrase() {
    return backupPhrase;
  }

  public void setBackupPhrase(List<String> backupPhrase) {
    this.backupPhrase = backupPhrase;
  }

  public NewWallet backupPhrase(List<String> backupPhrase) {
    this.backupPhrase = backupPhrase;
    return this;
  }

  public NewWallet addBackupPhraseItem(String backupPhraseItem) {
    this.backupPhrase.add(backupPhraseItem);
    return this;
  }

 /**
   * Optional spending password to encrypt / decrypt private keys
   * @return spendingPassword
  **/
  public String getSpendingPassword() {
    return spendingPassword;
  }

  public void setSpendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
  }

  public NewWallet spendingPassword(String spendingPassword) {
    this.spendingPassword = spendingPassword;
    return this;
  }

 /**
   * Desired assurance level based on the number of confirmations counter of each transaction.
   * @return assuranceLevel
  **/
  public String getAssuranceLevel() {
    if (assuranceLevel == null) {
      return null;
    }
    return assuranceLevel.value();
  }

  public void setAssuranceLevel(AssuranceLevelEnum assuranceLevel) {
    this.assuranceLevel = assuranceLevel;
  }

  public NewWallet assuranceLevel(AssuranceLevelEnum assuranceLevel) {
    this.assuranceLevel = assuranceLevel;
    return this;
  }

 /**
   * Wallet&#39;s name
   * @return name
  **/
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public NewWallet name(String name) {
    this.name = name;
    return this;
  }

 /**
   * Create a new wallet or Restore an existing one
   * @return operation
  **/
  public String getOperation() {
    if (operation == null) {
      return null;
    }
    return operation.value();
  }

  public void setOperation(OperationEnum operation) {
    this.operation = operation;
  }

  public NewWallet operation(OperationEnum operation) {
    this.operation = operation;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class NewWallet {\n");
    
    sb.append("    backupPhrase: ").append(toIndentedString(backupPhrase)).append("\n");
    sb.append("    spendingPassword: ").append(toIndentedString(spendingPassword)).append("\n");
    sb.append("    assuranceLevel: ").append(toIndentedString(assuranceLevel)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    operation: ").append(toIndentedString(operation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

