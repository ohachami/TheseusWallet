package com.hashami.cardano.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

public class Wallet  {
  
 /**
   * Unique wallet identifier  
  **/
  private String id = null;

 /**
   * Wallet's name  
  **/
  @NotNull
  private String name = null;

 /**
   * Current balance, in ADA  
  **/
  private BigDecimal balance = null;
  
  private Boolean hasSpendingPassword = null;

  private Date spendingPasswordLastUpdate = null;

  private Date createdAt = null;
  
  private List<Account> accounts;
 /**
   * Unique wallet identifier
   * @return id
  **/
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Wallet id(String id) {
    this.id = id;
    return this;
  }

 /**
   * Wallet&#39;s name
   * @return name
  **/
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Wallet name(String name) {
    this.name = name;
    return this;
  }

 /**
   * Current balance, in ADA
   * maximum: 45000000000000000
   * @return balance
  **/
  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  public Wallet balance(BigDecimal balance) {
    this.balance = balance;
    return this;
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Wallet {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    balance: ").append(toIndentedString(balance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private static String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

/* (non-Javadoc)
 * @see java.lang.Object#hashCode()
 */
@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
}

/* (non-Javadoc)
 * @see java.lang.Object#equals(java.lang.Object)
 */
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Wallet other = (Wallet) obj;
	if (id == null) {
		if (other.id != null)
			return false;
	} else if (!id.equals(other.id))
		return false;
	return true;
}

/**
 * @return the accounts
 */
public List<Account> getAccounts() {
	return accounts;
}

/**
 * @param accounts the accounts to set
 */
public void setAccounts(List<Account> accounts) {
	this.accounts = accounts;
}

/**
 * @return the hasSpendingPassword
 */
public Boolean getHasSpendingPassword() {
	return hasSpendingPassword;
}

/**
 * @param hasSpendingPassword the hasSpendingPassword to set
 */
public void setHasSpendingPassword(Boolean hasSpendingPassword) {
	this.hasSpendingPassword = hasSpendingPassword;
}

/**
 * @return the spendingPasswordLastUpdate
 */
public Date getSpendingPasswordLastUpdate() {
	return spendingPasswordLastUpdate;
}

/**
 * @param spendingPasswordLastUpdate the spendingPasswordLastUpdate to set
 */
public void setSpendingPasswordLastUpdate(Date spendingPasswordLastUpdate) {
	this.spendingPasswordLastUpdate = spendingPasswordLastUpdate;
}

/**
 * @return the createdAt
 */
public Date getCreatedAt() {
	return createdAt;
}

/**
 * @param createdAt the createdAt to set
 */
public void setCreatedAt(Date createdAt) {
	this.createdAt = createdAt;
}
}

