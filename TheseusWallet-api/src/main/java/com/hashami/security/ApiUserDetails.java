package com.hashami.security;

import java.util.Collection;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.hashami.dto.UserBean;

public class ApiUserDetails implements UserDetails{

	private static final long serialVersionUID = -2851968635289086607L;
	protected static final Logger LOGGER = LoggerFactory.getLogger(ApiUserDetails.class);
	private Collection<GrantedAuthority> authorities;
	private UserBean user;
	public ApiUserDetails(UserBean user, Collection<GrantedAuthority> auth) {
		this.authorities = auth;
		this.user = user;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public String getPassword() {
		return this.user.getPassword();
	}

	@Override
	public String getUsername() {
		return this.user.getLogin();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return BooleanUtils.isTrue(this.user.getActive());
	}
	
	public String getFullName() {
		return user.getFirstName().concat(" ").concat(user.getLastName());
	}
	
	public String getSharedKey() {
		return user.getSharedKey();
	}
	
	public boolean is2faActive() {
		return BooleanUtils.isTrue(user.getActive2fa());
	}
	
	public void activate2fa() {
		user.setActive2fa(true);
	}
	
	public void deactivate2fa() {
		user.setActive2fa(false);
	}
	
	public String getAuthorizations() {
		if(authorities != null && authorities.size()>0) {
			return StringUtils.join(authorities, ",");
		}
		return null;
	}

}
