package com.hashami.security;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class SecurityUtils {

	private static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(12);
	private static String SALT = "NoMoreCry";
	/**
	 * generate Bcrypt Hash
	 * @param message
	 * @return
	 */
	public static String bcrypt(String message) {
		return passwordEncoder.encode(message);
	}
	
	public static boolean bcryptMatches(String in, String persisted) {
		return passwordEncoder.matches(in, persisted);
	}
	
	/**
	 * generate SALT
	 * @return
	 */
	public static String generateSalt() {
		Random generator = new SecureRandom();
		byte[] salt = new byte[32];
		generator.nextBytes(salt);
		return Base64.encodeBase64String(salt);
	}
	
	public static String aesEncrypt(String login, String password, String toEncrypt) throws UnsupportedEncodingException {
		byte[] content = performEncryption(login, password, toEncrypt.getBytes("UTF-8"), Cipher.ENCRYPT_MODE);
    	return Base64.encodeBase64String(content);
	}
	
	public static String aesDecrypt(String login, String password, String toDecrypt) throws UnsupportedEncodingException {
		return new String(performEncryption(login, password, Base64.decodeBase64(toDecrypt.getBytes("UTF-8")), Cipher.DECRYPT_MODE), "UTF-8");
		
	}
	
	private static byte[] performEncryption(String login, String password, byte[] content, int operation) {
		try {
			
			byte[] aesKey = new byte[16];
			byte[] initVector = new byte[16];
			String randomVector = login.concat(password);
			byte[] tempVector = (randomVector).getBytes("UTF-8");
			String generatedKey  = SALT.concat(randomVector);
			byte[] tempKey = generatedKey.getBytes("UTF-8");
			
			if(tempVector.length > 16) {
				System.arraycopy(tempVector, 0, initVector, 0, 16);
			}else {
				int paddingCount = 16 - tempVector.length;
				initVector = randomVector.concat(StringUtils.repeat("0", paddingCount)).getBytes("UTF-8");
			}
			
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			
			tempKey = sha.digest(tempKey);
			
			if(tempKey.length > 16) {
				System.arraycopy(tempKey, 0, aesKey, 0, 16);
			}else {
				int paddingCount = 16 - tempKey.length;
				aesKey = generatedKey.concat(StringUtils.repeat("0", paddingCount)).getBytes("UTF-8");
			}
			

			SecretKeySpec secretKeySpec = new SecretKeySpec(aesKey, "AES");
			
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		    cipher.init(operation, secretKeySpec, new IvParameterSpec(initVector));
		    byte[] result = cipher.doFinal(content);
		    return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
