package com.hashami.service;

import java.util.List;

import com.hashami.cardano.dto.Account;
import com.hashami.persistance.cardano.model.CaAccount;
import com.hashami.persistance.dao.IBasicDAOService;

public interface IAccountService extends IBasicDAOService<CaAccount, Account>{

	Account getUserAccount(String login);
	
	List<Account> getUserAccounts(String login);
}
