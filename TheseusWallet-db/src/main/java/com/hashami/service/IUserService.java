package com.hashami.service;

import java.io.UnsupportedEncodingException;

import com.hashami.dto.ErrorBean;
import com.hashami.dto.UserBean;
import com.hashami.persistance.cardano.model.User;
import com.hashami.persistance.dao.IBasicDAOService;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

public interface IUserService extends IBasicDAOService<User, UserBean>{
	
	GoogleAuthenticatorKey createUser(UserBean toCreate, String activationToken) throws UnsupportedEncodingException;
	
	ErrorBean verifyActivationToken(String login, String token);
	
}
