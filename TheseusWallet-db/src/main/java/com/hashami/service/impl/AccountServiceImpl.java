package com.hashami.service.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hashami.cardano.dto.Account;
import com.hashami.persistance.cardano.model.CaAccount;
import com.hashami.persistance.dao.BasicDAOServiceImpl;
import com.hashami.persistance.utils.ExampleBuilder;
import com.hashami.service.IAccountService;

@Service
@Transactional
public class AccountServiceImpl extends BasicDAOServiceImpl<CaAccount, Account> implements IAccountService{

	public Account getUserAccount(String login) {
		
		List<Account> results = getUserAccounts(login);
		return results != null && !results.isEmpty() ? results.get(0) : null;
	}

	public List<Account> getUserAccounts(String login) {
		DetachedCriteria detachedCriteria = ExampleBuilder.buildCriteria(ExampleBuilder.buildExample(new CaAccount()));
		detachedCriteria.createAlias("user", "user", JoinType.LEFT_OUTER_JOIN);
		detachedCriteria.add(Restrictions.eq("user.login", login));
		detachedCriteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		List<CaAccount> results = getDao().getListByExemple(null, null, null, detachedCriteria);
		return results != null && !results.isEmpty() ? collectionMapper.mapCollection(results, Account.class) : null;
	}

}
