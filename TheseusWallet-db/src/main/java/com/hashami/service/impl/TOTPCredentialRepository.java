package com.hashami.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hashami.dto.UserBean;
import com.hashami.service.IUserService;
import com.warrenstrange.googleauth.ICredentialRepository;

@Service
public class TOTPCredentialRepository implements ICredentialRepository{

	@Autowired
	private IUserService userService;
	
	public String getSecretKey(String userName) {
		if(StringUtils.isNotBlank(userName)) {
			UserBean user = userService.findById(userName);
			return user != null ? user.getSharedKey() : null;
		}
		return null;
	}

	public void saveUserCredentials(String userName, String secretKey, int validationCode, List<Integer> scratchCodes) {
		if(StringUtils.isNotBlank(userName)) {
			UserBean user = userService.findById(userName);
			if(user != null) {
				user.setSharedKey(secretKey);
				userService.saveOrUpdate(user);
			}
		}
	}

}
