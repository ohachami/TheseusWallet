package com.hashami.service.impl;

import java.io.UnsupportedEncodingException;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hashami.cardano.dto.ResponseStatus;
import com.hashami.dto.ErrorBean;
import com.hashami.dto.UserBean;
import com.hashami.persistance.cardano.model.User;
import com.hashami.persistance.dao.BasicDAOServiceImpl;
import com.hashami.security.SecurityUtils;
import com.hashami.service.IUserService;
import com.warrenstrange.googleauth.GoogleAuthenticator;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

@Service
@Transactional
public class UserServiceImpl extends BasicDAOServiceImpl<User, UserBean> implements IUserService {

	public GoogleAuthenticatorKey createUser(UserBean toCreate, String activationToken) throws UnsupportedEncodingException{
		String clearPassword = toCreate.getPassword();
		toCreate.setPassword(SecurityUtils.bcrypt(clearPassword));
		toCreate.setActive(false);
		toCreate.setActive2fa(true);
		
		GoogleAuthenticator gAuth = new GoogleAuthenticator();
		GoogleAuthenticatorKey key = gAuth.createCredentials();
		String secretKey = SecurityUtils.aesEncrypt(toCreate.getLogin(), clearPassword, key.getKey());
		toCreate.setSharedKey(secretKey);
		
		toCreate = saveOrUpdate(toCreate);
		return key;
	}

	public ErrorBean verifyActivationToken(String login, String token) {
		UserBean user = findById(login);
		//TODO perform verification
		ErrorBean result = new ErrorBean("0000", ResponseStatus.success.name());
		return result;
	}

}
