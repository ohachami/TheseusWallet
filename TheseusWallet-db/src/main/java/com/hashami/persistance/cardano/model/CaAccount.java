package com.hashami.persistance.cardano.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CA_ACCOUNT")
public class CaAccount {

	private Integer id;
	/**
	 * Account's index in the wallet, starting at 0
	 **/
	private Long index = null;
	/**
	 * Available funds, in ADA
	 **/
	private BigDecimal amount = null;

	/**
	 * Account's name
	 **/
	private String name = null;

	private User user;
	/**
	 * @return the index
	 */
	@Column(name="CA_INDEX")
	public Long getIndex() {
		return index;
	}

	/**
	 * @param index
	 *            the index to set
	 */
	public void setIndex(Long index) {
		this.index = index;
	}

	/**
	 * @return the name
	 */
	@Column(name="NAME")	
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the balance
	 */
	@Column(name="BALANCE")
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param balance the balance to set
	 */
	public void setAmount(BigDecimal balance) {
		this.amount = balance;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID", nullable = false, insertable = true, updatable = false)
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
}
