package com.hashami.persistance.cardano.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "USER")
public class User {

	private String login;
	private String password;
	private String firstName;
	private String lastName;
	private String sharedKey;
	private Boolean active;
	private Boolean active2fa;
	private List<CaAccount> accounts;
	private Device device;
	
	/**
	 * @return the login
	 */
	@Id
	@Column(name="LOGIN")
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}
	/**
	 * @return the password
	 */
	@Column(name="PASSWORD")
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the firstName
	 */
	@Column(name="FIRST_NAME")
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return the lastName
	 */
	@Column(name="LAST_NAME")
	public String getLastName() {
		return lastName;
	}
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the sharedKey
	 */
	@Column(name="SHARED_KEY")
	public String getSharedKey() {
		return sharedKey;
	}
	/**
	 * @param sharedKey the sharedKey to set
	 */
	public void setSharedKey(String sharedKey) {
		this.sharedKey = sharedKey;
	}
	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	/**
	 * @return the active2fa
	 */
	@Column(name="ACTIVE_2FA")
	public Boolean getActive2fa() {
		return active2fa;
	}
	/**
	 * @param active2fa the active2fa to set
	 */
	public void setActive2fa(Boolean active2fa) {
		this.active2fa = active2fa;
	}
	/**
	 * @return the device
	 */
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "user", fetch=FetchType.EAGER)
	public Device getDevice() {
		return device;
	}
	/**
	 * @param device the device to set
	 */
	public void setDevice(Device device) {
		this.device = device;
	}
	/**
	 * @return the accounts
	 */
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "user")
	public List<CaAccount> getAccounts() {
		return accounts;
	}
	/**
	 * @param accounts the accounts to set
	 */
	public void setAccounts(List<CaAccount> accounts) {
		this.accounts = accounts;
	}
	
	
	
}
