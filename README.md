<h1>Introduction</h1>

Theseus Wallet is a backend for Cardano blockchain, basically it communicate 
with an instance of Cardano-SL (or Daedalus) using Cardano API V1.

Theseus provide out of the box users and accounts management, it's a fully operational backend
making easy for developpers to build mobile apps or accounting system arround Cardano

<h1>Features</h1>
<ul>
<li>2FA authentication support with Google Authenticator or any TOTP client</li>
<li>High level of security including password hashing, shared key encryption and JWT authentication</li>
<li>Cardano single transaction</li>
<li>Cardano multiple payment transaction (To be done)</li>
</ul>
<h1>Contribution</h1>
Any person who wants to get involved and contribute can make pull requests. <br/>
A road map gonna be setted up, with the up to come features, contributers can take the responsability of building one or more features
