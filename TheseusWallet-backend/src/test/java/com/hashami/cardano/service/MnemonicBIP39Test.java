package com.hashami.cardano.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.hashami.cardano.service.impl.MnemonicService;

import io.github.novacrypto.bip39.MnemonicValidator;
import io.github.novacrypto.bip39.Words;
import io.github.novacrypto.bip39.Validation.InvalidChecksumException;
import io.github.novacrypto.bip39.Validation.InvalidWordCountException;
import io.github.novacrypto.bip39.Validation.UnexpectedWhiteSpaceException;
import io.github.novacrypto.bip39.Validation.WordNotFoundException;
import io.github.novacrypto.bip39.wordlists.English;

public class MnemonicBIP39Test {

	@Test
	public void generateMnemonicTest() {
		MnemonicService service = new MnemonicService();
		List<CharSequence> mnemonics = service.getRandomMnemonics(Words.TWELVE.byteLength());
		try {
			MnemonicValidator
			.ofWordList(English.INSTANCE)
			.validate(mnemonics);
			Assert.assertTrue("Validation SUCCESS", true);
		} catch (InvalidChecksumException | InvalidWordCountException | WordNotFoundException
				| UnexpectedWhiteSpaceException e) {
			Assert.assertTrue("Validation error", false);
			e.printStackTrace();
		}
	}
}
