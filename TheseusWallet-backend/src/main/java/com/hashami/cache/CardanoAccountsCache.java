package com.hashami.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashami.cardano.service.CardanoService;
import com.hashami.cardano.v1.api.AccountsApi;
import com.hashami.cardano.v1.dto.Account;
import com.hashami.cardano.v1.dto.WalletResponseAccount;


public class CardanoAccountsCache extends CardanoService{

	private static final Logger LOGGER = LoggerFactory.getLogger(CardanoAccountsCache.class);
	private Map<Long, Account> accountsMap = new HashMap<>();
	
	public void init() {
		WalletResponseAccount response = null;//accountsApi.apiV1WalletsWalletIdAccountsGet(walletId, null, null);
		if(response != null) {
		   		 
	   		if(response.getData() != null && !response.getData().isEmpty()) {
	   			List<Account> accounts = collectionMapper.mapCollection(response.getData(), Account.class);
	   			synchronized (accountsMap) {
	   				//grouping by the wallet id part of the account id
	   				accountsMap = accounts.stream().collect(Collectors.toMap(a -> a.getIndex(), Function.identity()));
				}
	   		}
		}else {
			LOGGER.error("Error when initializing accounts cache");
		}
	}
	
	public Account getAccount(Integer accountId) {
		if(accountId != null) {
			return get(walletId);
		}
		return null;
	}
	
	/**
	 * @param key
	 * @return
	 * @see java.util.Map#get(java.lang.Object)
	 */
	public Account get(Object key) {
		return accountsMap.get(key);
	}

	public List<Account> getAll() {
		return new ArrayList<>(accountsMap.values());
	}
	/**
	 * @return the accountsApi
	 */
	public AccountsApi getAccountsApi() {
		return accountsApi;
	}

	/**
	 * @param accountsApi the accountsApi to set
	 */
	public void setAccountsApi(AccountsApi accountsApi) {
		this.accountsApi = accountsApi;
	}
}
