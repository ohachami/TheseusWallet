package com.hashami.cardano.backend.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.hashami.dto.ErrorBean;
import com.hashami.service.IUserService;

@Configurable
public class AccountActivation extends HttpServlet{

	@Autowired
	private IUserService userService;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String login = req.getParameter("accountId");
		String uuidToken = req.getParameter("activationToken");

		if(StringUtils.isNoneBlank(login) && 
				StringUtils.isNoneBlank(uuidToken)) {
			ErrorBean result = userService.verifyActivationToken(login, uuidToken);

			//token is ok
			if("0000".equals(result.getCode())) {
				//TODO activate account 
			}else {
				//TODO show error page with message
			}
		}
	}
	
	@Override
	public void init() throws ServletException {
		super.init();
		SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
		
	}
}
