package com.hashami.cardano.backend.filter;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.rs.security.jose.common.JoseException;
import org.apache.cxf.rs.security.jose.jaxrs.JwtAuthenticationFilter;
import org.apache.cxf.rs.security.jose.jwt.JwtException;
import org.apache.cxf.rs.security.jose.jwt.JwtToken;
import org.apache.cxf.rs.security.jose.jwt.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hashami.dto.ErrorBean;

public class CardanoAuthentificationFilter extends JwtAuthenticationFilter  {

	private static final Logger LOGGER = LoggerFactory.getLogger(CardanoAuthentificationFilter.class);
	private boolean audianceValidation = true;
	
	 @Context
	 private HttpServletRequest request;
	 
	public CardanoAuthentificationFilter() {}
	
	public CardanoAuthentificationFilter(String roleClaim) {
		super.setRoleClaim(roleClaim);
	}
	
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		try {
		super.filter(requestContext);
		}catch(JoseException e) {
			ErrorBean error = new ErrorBean("9999", e.getMessage());
			LOGGER.error("Error when processing token", e);
			requestContext.abortWith(Response.ok(error).status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).header("content-type", "application/json").build());
		}
	}
	
    @Override
    protected void validateToken(JwtToken jwt) {
    	String ip = (String)jwt.getClaims().getProperty("REMOTE_ADDR");
        if(StringUtils.isBlank(ip) || !StringUtils.equals(ip, request.getRemoteAddr())) {
        	throw new JwtException("IP Address invalid");
        }
        //TODO validate TOTP token
        
    	JwtUtils.validateTokenClaims(jwt.getClaims(), getTtl(), getClockOffset(), audianceValidation);
    }

	/**
	 * @return the audianceValidation
	 */
	public boolean isAudianceValidation() {
		return audianceValidation;
	}

	/**
	 * @param audianceValidation the audianceValidation to set
	 */
	public void setAudianceValidation(boolean audianceValidation) {
		this.audianceValidation = audianceValidation;
	}
}
