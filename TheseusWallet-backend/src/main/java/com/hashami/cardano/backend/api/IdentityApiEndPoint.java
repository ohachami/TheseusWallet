package com.hashami.cardano.backend.api;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.rs.security.jose.jwt.JoseJwtProducer;
import org.apache.cxf.rs.security.jose.jwt.JwtToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.hashami.cardano.api.IDentityApi;
import com.hashami.cardano.dto.AuthenticationResponse;
import com.hashami.cardano.dto.ResponseStatus;
import com.hashami.cardano.service.INotificationService;
import com.hashami.dto.UserBean;
import com.hashami.security.ApiAuthenticatorService;
import com.hashami.security.ApiUserDetails;
import com.hashami.security.SecurityUtils;
import com.hashami.service.IUserService;
import com.warrenstrange.googleauth.GoogleAuthenticatorKey;

@Service("identityEndpoint")
public class IdentityApiEndPoint implements IDentityApi {

	@Autowired
	private IUserService userService;

	@Autowired
	private ApiAuthenticatorService apiAuthenticatorService;
	
	@Autowired
	@Qualifier(value="mailService")
	private INotificationService notificationService;

	@Context
	private HttpServletRequest request;

	private static final Logger LOGGER = LoggerFactory.getLogger(IdentityApiEndPoint.class);

	@Override
	public Response registerUser(@NotNull @Valid UserBean auth) {
		AuthenticationResponse response = new AuthenticationResponse();
		try {
			if (auth != null && StringUtils.isNoneBlank(auth.getLogin())) {
				UserBean foundUser = userService.findById(auth.getLogin());
				if (foundUser == null) {
					
					String activationToken = UUID.randomUUID().toString();
					// generating shared key for new user
					GoogleAuthenticatorKey key = userService.createUser(auth, activationToken);

					String mailBody = IOUtils.readStringFromStream(IdentityApiEndPoint.class.getClassLoader().getResourceAsStream("emails/activation_email.html"));
					mailBody = mailBody.replace("{user.firstName}", auth.getFirstName());
					mailBody = mailBody.replace("{user.activationLink}", activationToken);
					// TODO send confirmation email with activation token
					notificationService.send("Theseus Wallet - Email verification", mailBody, auth.getLogin());
					
					response.setSharedKey(key.getKey());
					response.setMessage("Account created !");
					response.setStatus(ResponseStatus.success);
					return Response.status(HttpServletResponse.SC_CREATED).entity(response).build();
				} else {

					// TODO message i18n user already exists
					response.setMessage("User already exists");
					response.setStatus(ResponseStatus.error);
					return Response.status(HttpServletResponse.SC_OK).entity(response).build();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(ResponseStatus.fail);
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).entity(response).build();
		}
		response.setStatus(ResponseStatus.fail);
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).entity(response).build();
	}

	@Override
	public Response authenticateUser(@NotNull @Valid UserBean auth) {
		JoseJwtProducer producer = new JoseJwtProducer();
		AuthenticationResponse response = new AuthenticationResponse();
		try {
			// Authenticate the user using the credentials provided
			ApiUserDetails details = (ApiUserDetails) apiAuthenticatorService.authenticate(auth.getLogin(),
					auth.getPassword(), null);

			if (SecurityUtils.bcryptMatches(auth.getPassword(), details.getPassword())) {
				// Issue a token for the user
				JwtToken token = apiAuthenticatorService.issueToken(details);
				token.getClaims().setProperty("REMOTE_ADDR", request.getRemoteAddr());
				String serializedToken = producer.processJwt(token);

				// Return the token on the response
				response.setToken(serializedToken);
				response.setStatus(ResponseStatus.success);
				return Response.ok(response).build();
			} else {
				response.setMessage("Wrong password");
				response.setStatus(ResponseStatus.error);
				return Response.status(HttpServletResponse.SC_UNAUTHORIZED).entity(response).build();
			}

		} catch (Exception e) {
			LOGGER.error("error during authentication", e);
			response.setStatus(ResponseStatus.error);
			return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR).entity(response).build();
		}
	}
}
