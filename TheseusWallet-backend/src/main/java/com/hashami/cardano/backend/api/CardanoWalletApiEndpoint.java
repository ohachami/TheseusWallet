package com.hashami.cardano.backend.api;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;

import org.apache.cxf.security.SecurityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.hashami.cache.CardanoAccountsCache;
import com.hashami.cardano.api.ICardanoApi;
import com.hashami.cardano.backend.utils.CardanoWalletUtils;
import com.hashami.cardano.dto.Account;
import com.hashami.cardano.dto.NewAccount;
import com.hashami.cardano.dto.NewAddress;
import com.hashami.cardano.dto.Payment;
import com.hashami.cardano.dto.PaymentSource;
import com.hashami.cardano.dto.ResponseStatus;
import com.hashami.cardano.dto.Wallet;
import com.hashami.cardano.dto.WalletResponseAccount;
import com.hashami.cardano.dto.WalletResponseTransaction;
import com.hashami.cardano.dto.WalletResponseUniqueAccount;
import com.hashami.cardano.dto.WalletResponseUniqueWallet;
import com.hashami.cardano.dto.WalletResponseWalletAddress;
import com.hashami.cardano.service.CardanoService;

public class CardanoWalletApiEndpoint extends CardanoService implements ICardanoApi{

	private static final Logger LOGGER = LoggerFactory.getLogger(CardanoWalletApiEndpoint.class);
	
	
	@Autowired
	private CardanoAccountsCache accountsCache;
	

	@Override
	public Response getWallet(@NotNull String id) {
		if(!hasRoles("WALLET_ADMIN")) {
			WalletResponseUniqueWallet response = collectionMapper.map(walletsApi.apiV1WalletsWalletIdGet(walletId), WalletResponseUniqueWallet.class);
			if(response.getData() != null) {
				WalletResponseAccount accountsResp = collectionMapper.map(accountsApi.apiV1WalletsWalletIdAccountsGet(walletId, null, null),
						WalletResponseAccount.class);
				response.getData().setAccounts(collectionMapper.mapCollection(accountsResp.getData(), Account.class));
				return Response.ok().entity(response).build();
			}
		}
		return Response.status(HttpServletResponse.SC_FORBIDDEN).entity(CardanoWalletUtils.getErrorResponse()).build();
	}

	@Override
	public Response verifyAddress(@NotNull String address) {
		//collectionMapper.map(addressesApi.apiV1AddressesAddressValidityGet(address), WalletResponseAddressValidity.class)
		return Response.ok().build();
	}

	@Override
	public Response createAddress(@Valid NewAddress body) {
		SecurityContext securityContext = getSecurityContext();
		Principal principal = securityContext.getUserPrincipal();
		
		Account account = accountService.getUserAccount(principal.getName());
		if(account != null) {
			body.setAccountIndex(account.getIndex());
			body.setWalletId(walletId);
			return Response.ok(collectionMapper.map(addressesApi.apiV1AddressesPost(
					collectionMapper.map(body, com.hashami.cardano.v1.dto.NewAddress.class)),
					WalletResponseWalletAddress.class)).build();
		}
		
		Object[] params = {principal.getName()};
		LOGGER.error("user {} doesen't have any account", params);
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
				.entity(CardanoWalletUtils.getAddressCreationErrorResponse()).build();
		
	}

	@Override
	public Response searchTransactions(String address,
			Integer page, Integer perPage) {
		
		SecurityContext securityContext = getSecurityContext();
		Principal principal = securityContext.getUserPrincipal();
		Account account = accountService.getUserAccount(principal.getName());
		if(account != null) {
			return Response.ok(collectionMapper.map(transactionsApi.apiV1TransactionsGet(walletId, account.getIndex(), 
					address, page, perPage, null, null, "DES[created_at]"), 
					WalletResponseTransaction.class)).build();
		}
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
				.entity(CardanoWalletUtils.getTrxErrorResponse()).build();
	}

	@Override
	public Response createBatchTransactions(@NotNull @Valid Payment body) {
		// TODO implements on v2
		return null;
	}

	@Override
	public Response createTransaction(@NotNull @Valid Payment payment) {
		SecurityContext securityContext = getSecurityContext();
		Principal principal = securityContext.getUserPrincipal();
		
		Account account = accountService.getUserAccount(principal.getName());
		if(account != null) {
			
			PaymentSource paymentSource = new PaymentSource();
			paymentSource.setWalletId(walletId);
			paymentSource.setAccountIndex(account.getIndex());
			payment.setSource(paymentSource);
			return Response.ok(collectionMapper.map(transactionsApi.apiV1TransactionsPost(collectionMapper.map(
					payment, com.hashami.cardano.v1.dto.Payment.class)), WalletResponseTransaction.class)).build();
			
		}
		Object[] params = {principal.getName()};
		LOGGER.error("user {} doesen't have any account", params);
		return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
				.entity(CardanoWalletUtils.getTrxErrorResponse()).build();
		
	}


	@Override
	public Response createAccount(NewAccount body) {
		SecurityContext securityContext = getSecurityContext();
		Principal principal = securityContext.getUserPrincipal();
		
		//check if user already has account, one account per user for V1
		List<Account> accounts = accountService.getUserAccounts(principal.getName());
		if(accounts == null || accounts.isEmpty()) {
			WalletResponseUniqueAccount result = createAccountForUser(principal.getName(), body);
			if(result != null)  {
				return Response.ok(result).build();
			}else {
				return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
						.entity(CardanoWalletUtils.getAccountErrorResponse()).build();
			}
		}
		return Response.status(HttpServletResponse.SC_FORBIDDEN)
				.entity(CardanoWalletUtils.getAccountErrorResponse()).build();
	}
	
	@Override
	public Response getAccounts() {
		if(hasRoles("WALLET_ADMIN")) {
			WalletResponseAccount result = collectionMapper.map(accountsApi.apiV1WalletsWalletIdAccountsGet(walletId, null, null),
				WalletResponseAccount.class);
			
			if(result != null)  {
				return Response.ok(result).build();
			}else {
				return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
						.entity(CardanoWalletUtils.getAccountErrorResponse()).build();
			}
		}
		return Response.status(HttpServletResponse.SC_FORBIDDEN)
				.entity(CardanoWalletUtils.getAccountErrorResponse()).build();
	}

	@Override
	public Response createWallet(Wallet wallet) {
		//TODO implement later on v2
		return Response.status(HttpServletResponse.SC_FORBIDDEN)
		.entity(CardanoWalletUtils.getErrorResponse()).build();
	}

	@Override
	public Response getUserAccounts() {
		SecurityContext securityContext = getSecurityContext();
		Principal principal = securityContext.getUserPrincipal();
		List<Account> results = new ArrayList<>();
		List<Account> accounts = accountService.getUserAccounts(principal.getName());
		WalletResponseAccount response = CardanoWalletUtils.getAccountResponse();
		if(accounts != null && !accounts.isEmpty()) {
			for(Account acc : accounts) {
				WalletResponseUniqueAccount result = collectionMapper.map(accountsApi.apiV1WalletsWalletIdAccountsAccountIdGet(walletId, 
						acc.getIndex()),
						WalletResponseUniqueAccount.class);
				
				if(result.getStatus().equals(ResponseStatus.success)) {
					results.add(result.getData());
				}else {
					return Response.status(HttpServletResponse.SC_INTERNAL_SERVER_ERROR)
							.entity(CardanoWalletUtils.getAccountErrorResponse()).build();
				}
			}
		}
		response.setData(results);
		return Response.ok(response).build();
	}

}
