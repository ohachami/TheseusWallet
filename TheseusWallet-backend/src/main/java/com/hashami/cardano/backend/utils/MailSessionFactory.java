package com.hashami.cardano.backend.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

public final class MailSessionFactory {

	
	private MailSessionFactory() {
	}
	
	
	public static Properties getSmtpProperties() throws IOException {
		Properties props = new Properties();
		try (final InputStream stream =
				MailSessionFactory.class.getClassLoader().getResourceAsStream("smtp.properties")) {
			props.load(stream);
			
		}
		return props;
	}
	public static Session getSessionFor(String mailUser, String mailPassword) throws IOException {
		
		Properties props = getSmtpProperties();
		//create Authenticator object to pass in Session.getInstance argument
		Authenticator auth = new Authenticator() {
			//override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(mailUser, mailPassword);
			}
		};
		return Session.getInstance(props);
	}
	
}
