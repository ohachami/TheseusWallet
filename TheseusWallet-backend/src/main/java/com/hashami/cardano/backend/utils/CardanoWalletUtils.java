package com.hashami.cardano.backend.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.hashami.cardano.dto.Metadata;
import com.hashami.cardano.dto.PaginationMetadata;
import com.hashami.cardano.dto.ResponseStatus;
import com.hashami.cardano.dto.Transaction;
import com.hashami.cardano.dto.WalletResponseAccount;
import com.hashami.cardano.dto.WalletResponseAddressValidity;
import com.hashami.cardano.dto.WalletResponseTransaction;
import com.hashami.cardano.dto.WalletResponseWallet;
import com.hashami.cardano.dto.WalletResponseWalletAddress;

public class CardanoWalletUtils {

	public static WalletResponseAddressValidity getAddressValidityErrorResponse() {
		WalletResponseAddressValidity respo = new WalletResponseAddressValidity();
		respo.setStatus(ResponseStatus.error);
		respo.setMeta(new Metadata());
		return respo;
	}
	
	public static WalletResponseWalletAddress getAddressCreationErrorResponse() {
		WalletResponseWalletAddress respo = new WalletResponseWalletAddress();
		respo.setStatus(ResponseStatus.error);
		respo.setMeta(new Metadata());
		return respo;
	}
	
	public static WalletResponseWallet getErrorResponse() {
		WalletResponseWallet respo = new WalletResponseWallet();
		respo.setStatus(ResponseStatus.error);
		respo.setMeta(new Metadata());
		return respo;
	}
	
	public static WalletResponseTransaction getTrxSuccessResponse(Integer totalPages, Integer page, Integer perPage, Transaction trx) {
		return getTrxSuccessResponse(totalPages, page, perPage, Arrays.asList(trx));
	}
	
	public static WalletResponseTransaction getTrxSuccessResponse(Integer totalPages, Integer page, Integer perPage, List<Transaction> trx) {
		WalletResponseTransaction respo = new WalletResponseTransaction();
		respo.setStatus(ResponseStatus.success);
		respo.setMeta(new Metadata());
		respo.setData(trx);
		PaginationMetadata pagination = new PaginationMetadata();
		pagination.setPage(page);
		pagination.setPerPage(perPage);
		if(totalPages != null && perPage != null) {
			pagination.setTotalEntries(BigDecimal.valueOf((long)(totalPages*perPage)));
		}
		if(totalPages != null) {
			pagination.setTotalPages(BigDecimal.valueOf((long)totalPages));
		}
		respo.getMeta().setPagination(pagination);
		return respo;
	}
	
	public static WalletResponseTransaction getTrxFailResponse() {
		WalletResponseTransaction respo = new WalletResponseTransaction();
		respo.setStatus(ResponseStatus.fail);
		respo.setMeta(new Metadata());
		return respo;
	}
	
	public static WalletResponseTransaction getTrxErrorResponse() {
		WalletResponseTransaction respo = new WalletResponseTransaction();
		respo.setStatus(ResponseStatus.error);
		respo.setMeta(new Metadata());
		return respo;
	}
	
	public static WalletResponseAccount getAccountErrorResponse() {
		WalletResponseAccount respo = new WalletResponseAccount();
		respo.setStatus(ResponseStatus.error);
		respo.setMeta(new Metadata());
		return respo;
	}
	
	public static WalletResponseAccount getAccountResponse() {
		WalletResponseAccount respo = new WalletResponseAccount();
		respo.setStatus(ResponseStatus.success);
		respo.setMeta(new Metadata());
		return respo;
	}
	
	public static WalletResponseWallet getFailResponse() {
		WalletResponseWallet respo = new WalletResponseWallet();
		respo.setStatus(ResponseStatus.fail);
		respo.setMeta(new Metadata());
		return respo;
	}
	
	public static WalletResponseWallet getSuccessResponse(Integer totalPages, Integer page, Integer perPage) {
		WalletResponseWallet respo = new WalletResponseWallet();
		respo.setData(new ArrayList<>());
		respo.setStatus(ResponseStatus.success);
		respo.setMeta(new Metadata());
		PaginationMetadata pagination = new PaginationMetadata();
		pagination.setPage(page);
		pagination.setPerPage(perPage);
		if(totalPages != null) {
			pagination.setTotalPages(BigDecimal.valueOf((long)totalPages));
		}
		respo.getMeta().setPagination(pagination);
		return respo;
	}
	
	public static String buildAccountId(String walletId, BigDecimal accountIndex) {
		StringBuilder accountId = new StringBuilder("\"");
		accountId.append(walletId).append("@").append(accountIndex).append("\"");
		return accountId.toString();
	}
}
