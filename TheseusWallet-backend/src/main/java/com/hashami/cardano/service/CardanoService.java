package com.hashami.cardano.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.cxf.jaxrs.utils.JAXRSUtils;
import org.apache.cxf.security.LoginSecurityContext;
import org.apache.cxf.security.SecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.hashami.cardano.dto.Account;
import com.hashami.cardano.dto.NewAccount;
import com.hashami.cardano.dto.ResponseStatus;
import com.hashami.cardano.dto.WalletResponseUniqueAccount;
import com.hashami.cardano.service.impl.MnemonicService;
import com.hashami.cardano.v1.api.AccountsApi;
import com.hashami.cardano.v1.api.AddressesApi;
import com.hashami.cardano.v1.api.TransactionsApi;
import com.hashami.cardano.v1.api.WalletsApi;
import com.hashami.dto.UserBean;
import com.hashami.persistance.mapper.CollectionMapperDecorator;
import com.hashami.service.IAccountService;
import com.hashami.service.IUserService;

public abstract class CardanoService {

	@Autowired
	protected CollectionMapperDecorator collectionMapper;
	@Autowired
	protected MnemonicService mnemonicService;
	
	@Autowired
	protected IAccountService accountService;
	@Autowired
	protected IUserService userService;
	
	protected AccountsApi accountsApi;
	
	protected AddressesApi addressesApi;
	
	protected TransactionsApi transactionsApi;
	
	protected WalletsApi walletsApi;
	
	@Value("${wallet.id}")
	protected String walletId;
	
	public SecurityContext getSecurityContext() {
		return JAXRSUtils.getCurrentMessage().get(SecurityContext.class);
	}
	
	protected boolean isUserAccount(String userLogin,  BigDecimal id) {
		UserBean user = userService.findById(userLogin);
		//verify that user own this wallet
		return (user.getAccounts().stream().filter(a -> id.equals(a.getIndex())).findAny().isPresent());
	}
	
	protected boolean hasRoles(String ...roles) {
		LoginSecurityContext context = (LoginSecurityContext) getSecurityContext();
		boolean hasRole = false;
		if(roles != null) {
			Iterator<String> iterator = Arrays.asList(roles).iterator();
			while(iterator.hasNext() && !hasRole) {
				hasRole = context.isUserInRole(iterator.next());
			}
		}
		return hasRole;
	}
	
	protected WalletResponseUniqueAccount createAccountForUser(String login, NewAccount body) {
		
		UserBean user = userService.findById(login);
		if(user != null) {
			WalletResponseUniqueAccount response = collectionMapper.map(accountsApi.apiV1WalletsWalletIdAccountsPost(walletId, 
					collectionMapper.map(body, com.hashami.cardano.v1.dto.NewAccount.class)), WalletResponseUniqueAccount.class);
			if(response.getStatus().equals(ResponseStatus.success)) {
				Account toSave = response.getData();
				toSave.setUser(user);
				accountService.saveOrUpdate(toSave);
				toSave.setUser(null);//wipe the user
			}
			return response;
		}
		return null;
	}

	/**
	 * @return the accountsApi
	 */
	public AccountsApi getAccountsApi() {
		return accountsApi;
	}

	/**
	 * @param accountsApi the accountsApi to set
	 */
	public void setAccountsApi(AccountsApi accountsApi) {
		this.accountsApi = accountsApi;
	}

	/**
	 * @return the addressesApi
	 */
	public AddressesApi getAddressesApi() {
		return addressesApi;
	}

	/**
	 * @param addressesApi the addressesApi to set
	 */
	public void setAddressesApi(AddressesApi addressesApi) {
		this.addressesApi = addressesApi;
	}

	/**
	 * @return the transactionsApi
	 */
	public TransactionsApi getTransactionsApi() {
		return transactionsApi;
	}

	/**
	 * @param transactionsApi the transactionsApi to set
	 */
	public void setTransactionsApi(TransactionsApi transactionsApi) {
		this.transactionsApi = transactionsApi;
	}

	/**
	 * @return the walletsApi
	 */
	public WalletsApi getWalletsApi() {
		return walletsApi;
	}

	/**
	 * @param walletsApi the walletsApi to set
	 */
	public void setWalletsApi(WalletsApi walletsApi) {
		this.walletsApi = walletsApi;
	}
	
}
