package com.hashami.cardano.service.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.hashami.cardano.backend.utils.MailSessionFactory;
import com.hashami.cardano.service.INotificationService;

@Service("mailService")
public class NotificationServiceImpl implements INotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);

	@Override
	public void send(String subject, String body, List<String> recipients) {

		try {
			LOGGER.info("Start sending mail to :" + recipients);
			Session session = MailSessionFactory.getSessionFor(null, null);
			session.setDebug(true);
			MimeMessage msg = new MimeMessage(session);
			// set message headers
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			msg.setFrom(new InternetAddress("no_reply@TheseusWallet.com", "NoReply-JD"));
			msg.setSubject(subject, "UTF-8");
			msg.setSentDate(new Date());
			msg.setContent(body, "text/html");
			msg.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(StringUtils.join(recipients, ","), false));

			Transport transport = session.getTransport("smtp");
			Properties props = MailSessionFactory.getSmtpProperties();
			transport.connect(props.getProperty("mail.smtp.host"), Integer.valueOf(props.getProperty("mail.smtp.port")),
					props.getProperty("mail.smtp.user"), props.getProperty("mail.smtp.password"));
			transport.sendMessage(msg, msg.getAllRecipients());
			transport.close();

		} catch (Exception e) {
			LOGGER.error("Error sending mail");
			LOGGER.error(e.getMessage());
		}
	}

	@Override
	public void send(String subject, String body, String recipient) {
		send(subject, body, Arrays.asList(recipient));
	}

}
