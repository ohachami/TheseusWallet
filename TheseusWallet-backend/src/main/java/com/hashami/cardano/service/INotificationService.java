package com.hashami.cardano.service;

import java.util.List;

public interface INotificationService {
	
	void send(String subject, String body, List<String> recipients);
	void send(String subject, String body, String recipient);
}
