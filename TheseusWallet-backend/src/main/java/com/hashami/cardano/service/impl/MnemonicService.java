package com.hashami.cardano.service.impl;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import io.github.novacrypto.bip39.MnemonicGenerator;
import io.github.novacrypto.bip39.wordlists.English;

@Service
public class MnemonicService {

	
	public List<CharSequence> getRandomMnemonics(int count) {
		List<CharSequence> randomGen = new ArrayList<>(count);
		byte[] entropy = new byte[count];
		new SecureRandom().nextBytes(entropy);
		new MnemonicGenerator(English.INSTANCE)
		    .createMnemonic(entropy, randomGen::add);
		randomGen = randomGen.stream().filter(r -> StringUtils.isNoneBlank(r)).collect(Collectors.toList());
		return randomGen;
	}
}
