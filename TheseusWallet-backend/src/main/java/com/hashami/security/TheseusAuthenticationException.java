package com.hashami.security;

public class TheseusAuthenticationException extends Exception{

	private static final long serialVersionUID = -4584702627695580612L;

	public  TheseusAuthenticationException(String message) {
		super(message);
	}
	
	public  TheseusAuthenticationException(String message, Throwable e) {
		super(message, e);
	}
}
