package com.hashami.security;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.rs.security.jose.jwt.JwtClaims;
import org.apache.cxf.rs.security.jose.jwt.JwtToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.hashami.dto.UserBean;
import com.hashami.service.IUserService;
import com.warrenstrange.googleauth.GoogleAuthenticator;

@Service
public class ApiAuthenticatorService {

	@Autowired
	protected static final Logger LOGGER = LoggerFactory.getLogger(ApiAuthenticatorService.class);

	@Autowired
	private IUserService userService;
	
	public UserDetails authenticate(final String username, String password, String totp)
               throws TheseusAuthenticationException {
		
		boolean totpValid = false;
		UserBean user = userService.findById(username);
		if(user == null) {
			throw new TheseusAuthenticationException("user with login : " + username+ " not found");
		}
		//check TOTP if 2FA is active
		if(BooleanUtils.isTrue(user.getActive2fa())) {
			if(StringUtils.isNumeric(totp)) {
				GoogleAuthenticator auth = new GoogleAuthenticator();
				totpValid = auth.authorize(user.getSharedKey(), Integer.valueOf(totp));
			}
			
			if(!totpValid) {
				throw new TheseusAuthenticationException("Invalid TOTP " + totp);
			}
		}
		
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.add(new ApiAutorisation("AUTHENTIFIED"));
		
		return new ApiUserDetails(user, authorities);
	}
	
	public JwtToken issueToken(ApiUserDetails user) {
		ZonedDateTime issuingDt = LocalDateTime.now().atZone(ZoneId.systemDefault());
        JwtClaims claims = new JwtClaims();
        claims.setIssuer("Theseus-Wallet");
        claims.setIssuedAt(issuingDt.toInstant().getEpochSecond());
        claims.setExpiryTime(issuingDt.plusHours(1L).toInstant().getEpochSecond());
        claims.setSubject(user.getUsername());
        claims.setProperty("role", user.getAuthorizations());
        //TODO audiences to externalize 
        claims.setAudiences((Arrays.asList("http://localhost:8080/CardanoWallet/services/api/v1/wallets",
        		"http://localhost:8080/CardanoWallet/services/api/v1/addresses")));
        JwtToken token = new JwtToken(claims);
        return token;
    } 

}
